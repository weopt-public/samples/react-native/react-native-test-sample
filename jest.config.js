module.exports = {
  preset: 'react-native',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  transformIgnorePatterns: [
    "node_modules/(?!((jest-)?react-native|react-navigation|react-navigation-(.*)|react-navigation-tabs|@react-navigation/.*))" 
  ],
  setupFilesAfterEnv: [
    "./src/setupTests.js"
  ],
  testMatch: [ "**/__tests__/**/*.[jt]s?(x)", "**/__specs__/**/?(*.)+(spec|test).[jt]s?(x)" ]
}
