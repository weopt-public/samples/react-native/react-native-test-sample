describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Proceed Signin', async () => {
    const email = 's.paquis@we-opt.com';
    const password = 'FakePassword';
    // Check Input Presence
    await expect(element(by.id('email-signin-input'))).toBeVisible();
    await expect(element(by.id('password-signin-input'))).toBeVisible();
    // Fill Input
    await element(by.id('email-signin-input')).typeText(email);
    await element(by.id('password-signin-input')).typeText(password);
    // Press Button
    await element(by.id('signin-button')).tap();

    // Check Welcome Text
    await expect(element(by.id('welcome-text'))).toBeVisible();
    await expect(element(by.id('welcome-text'))).toHaveText(`Welcome ${email}`);
  });
});
