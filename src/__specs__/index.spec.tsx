import React from 'react';
import { mount, ReactWrapper } from "enzyme";
import AppRoot from '../index';


describe('End To End Test', () => {
  let component: ReactWrapper;
  beforeEach(() => {
    component = mount(<AppRoot />);
  });

  it('Signin Workflow Test', async (done) => {
    /***********************/
    /*** FILL SIGNIN FORM */
    /***********************/
    // Insert some value inside email Field
    const email = "test@we-opt.com";
    // Select TextInput
    const emailInput = component
      .find("TextInput")
      .findWhere((node: any) => node.props().testID === "email-signin-input");
    // Put value in TextInput
    emailInput.at(0).props().onChangeText(email);

    // Insert some value inside password Field
    const password = "password";
    // Select TextInput
    const passwordInput = component
      .find("TextInput")
      .findWhere(
        (node: any) => node.props().testID === "password-signin-input"
      );
    // Put value in TextInput
    passwordInput.at(0).props().onChangeText(password);

    component.update();

    // Press Signin Button
    const submitButton = component
      .find("TouchableOpacity")
      .findWhere((node: any) => node.props().testID === "signin-button");

    submitButton.first().props().onPress();

    /********************************/
    /*** Assert Correct Redirection */
    /********************************/

    setTimeout(() => {
        component.update();
        const welcomeText = component
            .find("Text")
            .findWhere((node: any) => node.props().testID === "welcome-text");
        expect(welcomeText.at(0).text()).toEqual(`Welcome ${email}`);
        done();
    }, 1000);
  });
});
