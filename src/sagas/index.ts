import { all } from 'redux-saga/effects';
import users from './users';
import auth from './auth';

export default function* root() {
  yield all([users(), auth()]);
}
