import { delay, put, takeEvery } from 'redux-saga/effects';
import * as types from '../actions/actionTypes';
import { signinFailure, signinSuccess } from '../actions/auth';
import { BaseAction } from '../types';


function* signin(action: BaseAction) {
  console.log('Intercepting Signin Request in Redux Sagas Middleware', action);
  const loggedUser = {
    id: 12,
    email: action.payload.email,
  }
  try {
    // Try to Signin
    // const authResponse = yield call(JSONPlaceholderAPI.signinRequest, action.payload);
    // console.log('Auth Response', authResponse);
    // We set the result in reducer
    yield delay(500);
    console.log('Dispatching a Signin Success');
    yield put(signinSuccess(loggedUser));
  } catch (e) {
    yield put(signinFailure(e.message));
  }
}

export default function* root() {
  yield takeEvery(types.AUTH.SIGNIN_REQUEST, signin);
}
