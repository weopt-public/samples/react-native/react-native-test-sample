import {
  createStackNavigator,
  createSwitchNavigator
} from "react-navigation";
import Home from "./containers/Home";
import Login from "./containers/Login";

const HomeStack = createStackNavigator({
  Home: {
    screen: Home
  }
});

const LoginStack = createStackNavigator({
  Login: {
    screen: Login
  }
});

const App = createSwitchNavigator(
  {
    HomeStack,
    LoginStack
  },
  {
    initialRouteName: "LoginStack"
  }
);

export default App;
