import { shallow, mount } from "enzyme";
import React from "react";
import MyButton from "../MyButton";
import rendered from 'react-test-renderer';

describe("MyButton tests", () => {
  // Mock functions
  const mockOnPress = jest.fn();

  test("Snapshot", () => {
    const tree = rendered.create(<MyButton onPress={mockOnPress} text="Button Text" />).toJSON();
    expect(tree).toMatchSnapshot();
  })

  test("Shallow Sample", () => {
    const component = shallow(<MyButton onPress={mockOnPress} text="Button Text" />);
    console.log(component.debug());
  })

  test("Mount Sample", () => {
    const component = mount(<MyButton onPress={mockOnPress} text="Button Text" />);
    console.log(component.debug());
  })

});