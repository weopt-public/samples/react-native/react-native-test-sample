import { shallow, ShallowWrapper } from "enzyme";
import React from "react";
import SigninForm from "../SigninForm";

describe("SigninForm tests", () => {
  let component: ShallowWrapper;

  // Mock functions
  const mockOnSigninfn = jest.fn();

  beforeEach(() => {
    component = shallow(<SigninForm onSignin={mockOnSigninfn} />);
    // console.log(component.debug());
  });

  test("Contains Two TextInput and One MyButton", () => {
    // Text Input
    expect(component.find("TextInput")).toHaveLength(2);

    // My Button
    expect(component.find("MyButton")).toHaveLength(1);
  });

  test("Fill the form and Submit", () => {
    // Insert some value inside email Field
    const email = "test@we-opt.com";
    // Select TextInput
    const emailInput = component
      .find("TextInput")
      .findWhere((node: any) => node.props().testID === "email-signin-input");
    // Put value in TextInput
    emailInput.props().onChangeText(email);

    // Insert some value inside password Field
    const password = "password";
    // Select TextInput
    const passwordInput = component
      .find("TextInput")
      .findWhere(
        (node: any) => node.props().testID === "password-signin-input"
      );
    // Put value in TextInput
    passwordInput.props().onChangeText(password);

    // Press Signin Button
    const submitButton = component
      .find("MyButton")
      .findWhere((node: any) => node.props().testID === "signin-button");

    submitButton.props().onPress();

    // Assert action
    expect(mockOnSigninfn.mock.calls.length).toEqual(1);
    // Check Parameters
    expect(mockOnSigninfn.mock.calls[0][0]).toEqual({ email, password });
  });
});
