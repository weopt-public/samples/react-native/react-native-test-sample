import React, { useState } from "react";
import { StyleSheet, TextInput, View } from "react-native";
import { SigninFormType } from "../types";
import MyButton from "./MyButton";

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center"
  },
  inputContainer: {
    justifyContent: "flex-start",
    alignItems: "flex-start",
    marginBottom: 5
  },
  inputBox: {
    height: 35,
    width: 300,
    borderRadius: 3,
    paddingLeft: 14,
    paddingRight: 14,
    backgroundColor: "rgba(151, 213, 227, 0.2)",
    color: "#FFF"
  },
  button: {
    width: 300,
    backgroundColor: "#43B99D",
    borderRadius: 10,
    marginVertical: 10,
    paddingVertical: 12
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#ffffff",
    textAlign: "center"
  },
  InvalidInput: {
    borderColor: "#FF0000",
    borderWidth: 1
  },
  InvalidPasswordEmailText: {
    color: "#FF0000",
    fontSize: 12
  }
});

type SigninFormProps = {
  onSignin: (data: SigninFormType) => {};
};

/**
 * Signin Form
 * @param props
 */
const SigninForm = ({ onSignin }: SigninFormProps) => {
  // Email Field
  const [email, setEmail] = useState();
  // Password Field
  const [password, setPassword] = useState();

  // On Submitting form
  const onSubmit = () => {
    console.log("Submiting Form", email, password);
    onSignin({ email, password });
  };

  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <TextInput
          testID="email-signin-input"
          value={email}
          onChangeText={setEmail}
          placeholder="Email"
          placeholderTextColor="#AEAEAE"
          style={styles.inputBox}
          keyboardType="email-address"
          returnKeyType="next"
        />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          testID="password-signin-input"
          value={password}
          onChangeText={setPassword}
          placeholder="Password"
          placeholderTextColor="#AEAEAE"
          style={styles.inputBox}
          secureTextEntry
          returnKeyType="next"
          onSubmitEditing={onSubmit}
        />
      </View>
      <MyButton testID="signin-button" onPress={onSubmit} text="SIGN IN" />
    </View>
  );
};

export default SigninForm;
