import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";

const styles = StyleSheet.create({
  button: {
    width: 300,
    backgroundColor: "#43B99D",
    borderRadius: 10,
    marginVertical: 10,
    paddingVertical: 12
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#ffffff",
    textAlign: "center"
  }
});

interface MyButtonProps {
  text: string;
  onPress: () => void;
  testID?: string;
}

const MyButton = (props: MyButtonProps) => (
  <TouchableOpacity testID={props.testID} style={styles.button} onPress={props.onPress}>
    <Text style={styles.buttonText}>{props.text}</Text>
  </TouchableOpacity>
);

export default MyButton;
