const apiEntry = 'https://jsonplaceholder.typicode.com';
const usersNamespace = 'users';
const authNamespace = 'auth';

export default class JSONPlacholderAPI {
  static apiEntry = apiEntry;
  static usersNamespace = usersNamespace;

  static async fetchUser(userID: any) {
    const url = `${apiEntry}/${usersNamespace}/${userID}`;
    return fetch(url).then(response => response.json());
  }

  static async signinRequest(credential: any) {
    const url = `${apiEntry}/${authNamespace}`;
    return fetch(url).then(response => response.json());
  }
}
