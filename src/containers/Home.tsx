import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Image } from "react-native";
import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState
} from "react-navigation";
import { connect } from "react-redux";
import { fetchUser } from "../actions/users";
import { logout } from "../actions/auth";
import { User } from "../types";
import MyButton from "../components/MyButton";
import logo from "../assets/WeOpt_NoText_white.png";

export interface Props {
  fetchUser: typeof fetchUser;
  user: User;
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

export class Home extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam("Title", "Welcome Board"),
    headerStyle: {
      backgroundColor: navigation.getParam("BackgroundColor", "#1d222e")
    },
    headerTintColor: navigation.getParam("HeaderTintColor", "#fff")
  });

  componentDidMount() {
    this.props.fetchUser("1");
  }

  componentWillReceiveProps(props) {
    if (!props.loggedUser) {
      this.props.navigation.navigate('LoginStack');
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={logo} style={styles.image} />
        <Text testID="welcome-text" style={styles.welcome} >Welcome {this.props.loggedUser && this.props.loggedUser.email}</Text>
        <MyButton onPress={this.props.logout} text="LOGOUT" />
      </View>
    );
  }
}

const mapStateToProps = (state: any) => ({
  user: state.users.user,
  loggedUser: state.auth.loggedUser
});

const mapDispatchToProps = {
  fetchUser,
  logout
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  image: {
    width: 256,
    height: 227
  },
});
