import React, { useCallback,useEffect } from "react";
import { StyleSheet, View, Image, Text, ActivityIndicator } from "react-native";
import SigninForm from "../components/SigninForm";
import { SigninFormType } from "../types";
import { useDispatch, useSelector } from "react-redux";
import { signinRequest } from "../actions/auth";
import logo from "../assets/WeOpt_NoText_white.png";

const Login = (props) => {
  const dispatch = useDispatch();
  // Know if Signin Request is Pending
  const isPending = useSelector((state: any) => state.auth.isPending);
  // Check Logged User
  const loggedUser = useSelector((state: any) => state.auth.loggedUser);
  
  // Dispatch a Signin Request Redux Action
  const onSubmitForm = (data: SigninFormType) => {
    console.log('Dispatching a Signin Request');
    dispatch(signinRequest(data));
  };

  useEffect(() => {
    if (loggedUser) {
      // Redirect To HOme
      console.log('Redirect to Home');
      props.navigation.navigate('HomeStack');
    }
  }, [loggedUser])

  return (
    <View style={styles.container}>
      <Image source={logo} style={styles.image} />
      {isPending ? (
        <ActivityIndicator size="large" color="#43B99D" />
      ) : (
        <SigninForm onSignin={onSubmitForm} />
      )}
      <Text style={styles.text}>Powered By Weopt</Text>
    </View>
  );
};

Login.navigationOptions = {
  header: null
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: "#1d222e"
  },
  image: {
    width: 256,
    height: 227
  },
  text: {
    color: "#fff"
  }
});

export default Login;
