declare module 'redux-persist/lib/*';

export interface BaseAction {
  type: string;
  payload: any;
}

export interface User {}

export interface SigninFormType {
  email: string,
  password: string
}