'use strict';
import { AUTH } from '../actions/actionTypes';

const initialState = {
  isPending: false,
  loggedUser: null,
};

export default function usersReducer(state = initialState, action: any = {}) {
  switch (action.type) {
    case AUTH.SIGNIN_REQUEST:
      return { ...state, isPending: true };
    case AUTH.SIGNIN_SUCCESS:
      return { ...state, loggedUser: action.payload, isPending: false };
    case AUTH.LOGOUT:
        return initialState;
    default:
      return state;
  }
}
