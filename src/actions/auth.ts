import { action } from 'typesafe-actions';
import { SigninFormType } from '../types';
import * as types from './actionTypes';

export const signinSuccess = (payload: any) =>
  action(types.AUTH.SIGNIN_SUCCESS, payload);

export const signinFailure = (payload: string) =>
  action(types.AUTH.SIGNIN_FAILURE, payload);
  
export const signinRequest = (payload: SigninFormType) => action(types.AUTH.SIGNIN_REQUEST, payload);

export const logout = () => action(types.AUTH.LOGOUT);
